INSERT INTO product (id, name, price, stock) VALUES (1, 'Apple', 5, 10);
INSERT INTO product (id, name, price, stock) VALUES (2, 'Orange', 6, 10);
INSERT INTO product (id, name, price, stock) VALUES (3, 'Banana', 3, 10);

INSERT INTO customer (id, username, password) VALUES (1, 'User1', 'Password1');
INSERT INTO customer (id, username, password) VALUES (2, 'User2', 'Password2');