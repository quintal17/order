package com.example.order.kafka.listener;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import static com.example.order.util.Constants.NEW_ORDER_TOPIC;
import static com.example.order.util.Constants.ORDER_GROUP;

@Component
public class NewOrderListener {
	@KafkaListener(topics = NEW_ORDER_TOPIC, groupId = ORDER_GROUP)
	public void listen(String message) {
		System.out.println("New order created, id : " + message);
	}
}
