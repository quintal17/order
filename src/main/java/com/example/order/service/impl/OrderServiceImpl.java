package com.example.order.service.impl;

import com.example.order.data.OrderData;
import com.example.order.data.ProductData;
import com.example.order.entity.Customer;
import com.example.order.entity.Order;
import com.example.order.entity.Product;
import com.example.order.exception.BadRequestException;
import com.example.order.exception.ResourceNotFoundException;
import com.example.order.exception.InsufficientStockException;
import com.example.order.repository.CustomerRepository;
import com.example.order.repository.OrderRepository;
import com.example.order.repository.ProductRepository;
import com.example.order.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static com.example.order.util.Constants.NEW_ORDER_TOPIC;

@Service
public class OrderServiceImpl implements OrderService {

	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private OrderRepository orderRepository;

	@Override
	public OrderData createOrder(OrderData orderData) {
		validateRequest(orderData);

		Customer customer = getCustomer(orderData);

		Set<ProductTotalPrice> productSet = getProducts(orderData);

		Order order = createOrder(customer, productSet);

		kafkaTemplate.send(NEW_ORDER_TOPIC, String.valueOf(order.getId()));

		return new OrderData(order);
	}

	private Customer getCustomer(OrderData orderData) {
		Optional<Customer> customerOpt = customerRepository.findById(orderData.getCustomer().getId());

		if(customerOpt.isEmpty()) {
			throw new ResourceNotFoundException("Customer not found for customer id : " + orderData.getCustomer().getId());
		}
		return customerOpt.get();
	}

	private Order createOrder(Customer customer, Set<ProductTotalPrice> productSet) {
		Order order = Order.builder()
				.products(productSet.stream().map(ps -> ps.product).collect(Collectors.toSet()))
				.customer(customer)
				.totalPrice(productSet.stream().mapToDouble(ps -> ps.totalPrice).sum())
				.build();

		order = orderRepository.save(order);
		return order;
	}

	private Set<ProductTotalPrice> getProducts(OrderData orderData) {

		List<Product> productList = productRepository
				.findAllById(orderData
						.getProducts()
						.stream()
						.map(ProductData::getId)
						.toList());

		Map<Long, Product> productMap = productList
				.stream()
				.collect(Collectors.toMap(Product::getId, product -> product));

		Set<ProductTotalPrice> productSet = new HashSet<>();
		orderData.getProducts().forEach(p -> {
			Product product = productMap.get(p.getId());

			if(product == null) {
				throw new ResourceNotFoundException("Item not found for item : " + p.getName());
			}

			if(product.getStock() < p.getQuantity()) {
				String errorMessage = String.format("Insufficient stock for %s, actual stock is %d",
						product.getName(),
						product.getStock());
				throw new InsufficientStockException(errorMessage);
			}

			productSet.add(new ProductTotalPrice(product, product.getPrice() * p.getQuantity()));
		});

		return productSet;
	}

	private void validateRequest(OrderData orderData) {

		if(orderData.getCustomer() == null || orderData.getCustomer().getId() == null) {
			throw new BadRequestException("Customer is required");
		}

		if(orderData.getProducts() == null || orderData.getProducts().isEmpty()) {
			throw new BadRequestException("Products are required");
		}

	}


	private record ProductTotalPrice(Product product, double totalPrice) {
	}
}
