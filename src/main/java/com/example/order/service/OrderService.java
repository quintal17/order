package com.example.order.service;

import com.example.order.data.OrderData;

/**
 * Interface for order service
 */
public interface OrderService {
	OrderData createOrder(OrderData orderData);
}
