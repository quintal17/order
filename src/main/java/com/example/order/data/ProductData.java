package com.example.order.data;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProductData {
	private Long id;
	private String name;
	private Long price;
	private Long stock;
	private Long quantity;
}
