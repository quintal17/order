package com.example.order.data;

import com.example.order.entity.Order;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;
import java.util.stream.Collectors;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OrderData {
	private Long id;
	private CustomerData customer;
	private Set<ProductData> products;
	private double totalPrice;

	public OrderData(Order order) {
		this.id = order.getId();
		this.customer = CustomerData.builder()
				.username(order.getCustomer().getUsername())
				.id(order.getCustomer().getId())
				.build();
		this.products = order.getProducts()
				.stream()
				.map(p -> ProductData.builder()
						.id(p.getId())
						.price(p.getPrice())
						.name(p.getName())
						.build())
				.collect(Collectors.toSet());
		this.totalPrice = order.getTotalPrice();
	}
}
