package com.example.order.data;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CustomerData {
	private Long id;
	private String username;
}
