package com.example.order.controller;

import com.example.order.data.OrderData;
import com.example.order.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/orders")
public class OrderController {

	@Autowired
	private OrderService orderService;

	@PostMapping
	ResponseEntity<OrderData> createOrder(@RequestBody OrderData orderData) {
		return new ResponseEntity<>(orderService.createOrder(orderData), HttpStatus.OK);
	}
}
