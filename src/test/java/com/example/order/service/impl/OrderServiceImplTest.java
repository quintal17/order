package com.example.order.service.impl;

import com.example.order.data.CustomerData;
import com.example.order.data.OrderData;
import com.example.order.data.ProductData;
import com.example.order.entity.Customer;
import com.example.order.entity.Product;
import com.example.order.exception.BadRequestException;
import com.example.order.exception.InsufficientStockException;
import com.example.order.exception.ResourceNotFoundException;
import com.example.order.repository.CustomerRepository;
import com.example.order.repository.OrderRepository;
import com.example.order.repository.ProductRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.kafka.core.KafkaTemplate;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.when;

class OrderServiceImplTest {
	@Mock
	private KafkaTemplate<String, String> kafkaTemplate;

	@Mock
	private CustomerRepository customerRepository;

	@Mock
	private ProductRepository productRepository;

	@Mock
	private OrderRepository orderRepository;

	@InjectMocks
	private OrderServiceImpl orderService;

	@BeforeEach
	void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	void testCreateOrder_Success() {

		// given
		OrderData orderData = new OrderData();
		orderData.setCustomer(CustomerData.builder().id(1L).build());
		orderData.setProducts(Set.of(ProductData.builder().id(1L).quantity(1L).build()));

		Customer mockCustomer = Customer.builder().id(1L).build();
		Product mockProduct = Product.builder().id(1L).price(10L).stock(10L).build();
		when(customerRepository.findById(1L)).thenReturn(Optional.of(mockCustomer));
		when(productRepository.findAllById(anyList())).thenReturn(List.of(mockProduct));
		when(orderRepository.save(any())).thenAnswer(invocation -> invocation.getArgument(0));


		// when
		OrderData createdOrderData = orderService.createOrder(orderData);

		// then
		assertNotNull(createdOrderData);
		assertEquals(1L, createdOrderData.getCustomer().getId());
		assertEquals(1, createdOrderData.getProducts().size());
		assertEquals(10.0, createdOrderData.getTotalPrice());
	}

	@Test
	void testCreateOrder_CustomerNotFound() {
		// given
		OrderData orderData = new OrderData();
		orderData.setProducts(Set.of(ProductData.builder().id(1L).quantity(1L).build()));
		orderData.setCustomer(CustomerData.builder().id(1L).build());

		when(customerRepository.findById(any())).thenReturn(Optional.empty());

		// when
		assertThrows(ResourceNotFoundException.class, () -> orderService.createOrder(orderData));

		// then
	}

	@Test
	void testCreateOrder_ProductNotFound() {
		// given
		OrderData orderData = new OrderData();
		orderData.setCustomer(CustomerData.builder().id(1L).build());
		orderData.setProducts(Set.of(ProductData.builder().id(1L).quantity(1L).build()));

		when(customerRepository.findById(1L)).thenReturn(Optional.of(Customer.builder().id(1L).build()));
		when(productRepository.findAllById(any())).thenReturn(Collections.emptyList());

		// when
		assertThrows(ResourceNotFoundException.class, () -> orderService.createOrder(orderData));

		// then
	}

	@Test
	void testCreateOrder_InsufficientStock() {
		// given
		OrderData orderData = new OrderData();
		orderData.setCustomer(CustomerData.builder().id(1L).build());
		orderData.setProducts(Set.of(ProductData.builder().id(1L).quantity(11L).build()));

		Customer mockCustomer = Customer.builder().id(1L).build();
		Product mockProduct = Product.builder().id(1L).stock(5L).build();
		when(customerRepository.findById(1L)).thenReturn(Optional.of(mockCustomer));
		when(productRepository.findAllById(any())).thenReturn(List.of(mockProduct));

		// when
		assertThrows(InsufficientStockException.class, () -> orderService.createOrder(orderData));

		// then
	}

	@Test
	void testCreateOrder_MissingCustomer() {
		OrderData orderData = new OrderData();

		assertThrows(BadRequestException.class, () -> orderService.createOrder(orderData));
	}

	@Test
	void testCreateOrder_EmptyProductsList() {
		OrderData orderData = new OrderData();
		orderData.setCustomer(CustomerData.builder().id(1L).build());
		orderData.setProducts(Collections.emptySet());

		assertThrows(BadRequestException.class, () -> orderService.createOrder(orderData));
	}
}