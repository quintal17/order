# ORDER API
## Prerequisites
Ensure you have the following installed on your machine:

- Docker
- Java 17
- Maven

## Getting Started

To run this project locally, follow these steps:
```sh
git clone <repository-url>
cd <repository-directory>
```
Start the kafka

```sh
docker-compose up
```

Run the api
```sh
mvn spring-boot:run
```

Access the API

The API will be available at http://localhost:8080/api/orders.

## Authentication

Make sure to include Basic Authentication credentials in your HTTP requests. Use the following credentials:

###### Username: admin
###### Password: password

## API Endpoint

###### URL : POST /api/orders
###### Request Body :
```json
{
  "customer": {
    "id": 1
  },
  "products": [
    {
      "id": 1,
      "name": "Apple",
      "quantity": 1
    },
    {
      "id": 2,
      "name": "Orange",
      "quantity": 5
    }
  ]
}
```

###### Response Body :
```json
{
  "id": 2,
  "customer": {
    "id": 1,
    "username": "User1"
  },
  "products": [
    {
      "id": 1,
      "name": "Apple",
      "price": 5,
      "stock": null,
      "quantity": null
    },
    {
      "id": 2,
      "name": "Orange",
      "price": 6,
      "stock": null,
      "quantity": null
    }
  ],
  "totalPrice": 35.0
}

```